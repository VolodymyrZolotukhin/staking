const Stake = artifacts.require("Stake.sol");
const TestCoin = artifacts.require("ERC20Mock.sol");
const TimeTravaler = require("ganache-time-traveler");
const Reverter = require("./helpers/reverter");
const truffleAssert = require("truffle-assertions");

contract("Stake", async (accounts) => {
  const OWNER = accounts[0];
  const ALICE = accounts[1];
  const RON = accounts[2];
  const BOB = accounts[3];
  const reverter = new Reverter(web3);
  const ONE_ETH = web3.utils.toBN(web3.utils.toWei("1", "ether"));
  const FIRST_PERIOD = 1000 * 60;
  const SECOND_PERIOD = FIRST_PERIOD * 5;
  const AMOUNT = "100";

  let testCoin;
  let staker;
  let snapshot;

  function revert() {
    reverter.revert();
    TimeTravaler.revertToSnapshot(snapshot);
  }

  function toBN(str) {
    return web3.utils.toBN(str);
  }

  before("setup", async () => {
    testCoin = await TestCoin.new();
    let ts = (await web3.eth.getBlock("latest"))["timestamp"];
    staker = await Stake.new(
      FIRST_PERIOD + ts,
      SECOND_PERIOD + ts,
      testCoin.address
    );

    await testCoin.mint(ONE_ETH, { from: ALICE });
    await testCoin.mint(ONE_ETH, { from: RON });
    await testCoin.mint(ONE_ETH, { from: BOB });

    await testCoin.approve(staker.address, AMOUNT * 3, { from: ALICE });
    await testCoin.approve(staker.address, AMOUNT * 3, { from: RON });
    await testCoin.approve(staker.address, AMOUNT * 3, { from: BOB });

    reverter.snapshot();
    snapshot = TimeTravaler.takeSnapshot();
  });

  afterEach("revert", revert);

  describe("stake", async () => {
    it("should stake 100 testCoins from ALICE", async () => {
      await staker.stake(AMOUNT, { from: ALICE });

      assert.equal(
        AMOUNT,
        (await testCoin.balanceOf(staker.address)).toString()
      );
      assert.equal(AMOUNT, await staker.totalStaked());
      assert.equal(AMOUNT, await staker.usersStaked(ALICE));
      assert.equal("0", await staker.totalSupply());
    });

    it("should stake 100 from ALICE, then stake 100 from RON", async () => {
      await staker.stake(AMOUNT, { from: ALICE });
      await staker.stake(AMOUNT, { from: RON });

      assert.equal(
        AMOUNT * 2,
        (await testCoin.balanceOf(staker.address)).toString()
      );
      assert.equal(AMOUNT * 2, await staker.totalStaked());
      assert.equal(AMOUNT, await staker.usersStaked(RON));
      assert.equal(AMOUNT, await staker.usersStaked(ALICE));
      assert.equal("0", await staker.totalSupply());
    });

    it("should stake 100 from ALICE, then stake 100 from RON, stake 100 from ALICE again", async () => {
      await staker.stake(AMOUNT, { from: ALICE });

      await staker.stake(AMOUNT, { from: RON });

      await staker.stake(AMOUNT, { from: ALICE });

      assert.equal(
        AMOUNT * 3,
        (await testCoin.balanceOf(staker.address)).toString()
      );
      assert.equal(AMOUNT * 3, await staker.totalStaked());
      assert.equal(AMOUNT * 2, await staker.usersStaked(ALICE));
      assert.equal(AMOUNT, await staker.usersStaked(RON));
      assert.equal("0", await staker.totalSupply());
    });

    it("should revert when try stake after deadline", async () => {
      TimeTravaler.advanceTime(FIRST_PERIOD + 1);

      await truffleAssert.reverts(
        staker.stake(AMOUNT, { from: ALICE }),
        "Unable to exec during this time period"
      );
    });
  });

  describe("addSupply", async () => {
    it("should add supply 100 from RON", async () => {
      await staker.addSupply(AMOUNT, { from: RON });

      assert.equal(
        AMOUNT,
        (await testCoin.balanceOf(staker.address)).toString()
      );
      assert.equal("0", await staker.totalStaked());
      assert.equal(AMOUNT, await staker.totalSupply());
      assert.equal("0", await staker.usersStaked(RON));
    });

    it("should add supply 100 from RON, then add supply from ALICE", async () => {
      await staker.addSupply(AMOUNT, { from: RON });

      await staker.addSupply(AMOUNT, { from: ALICE });

      assert.equal(
        AMOUNT * 2,
        (await testCoin.balanceOf(staker.address)).toString()
      );
      assert.equal("0", await staker.totalStaked());
      assert.equal(AMOUNT * 2, await staker.totalSupply());
      assert.equal("0", await staker.usersStaked(RON));
      assert.equal("0", await staker.usersStaked(ALICE));
    });

    it("should add supply 100 from RON after stake deadline", async () => {
      TimeTravaler.advanceTime(FIRST_PERIOD);

      await staker.addSupply(AMOUNT, { from: RON });

      assert.equal(
        AMOUNT,
        (await testCoin.balanceOf(staker.address)).toString()
      );
      assert.equal("0", await staker.totalStaked());
      assert.equal(AMOUNT, await staker.totalSupply());
      assert.equal("0", await staker.usersStaked(RON));
    });

    it("should revert when try to add supply after lock deadline", async () => {
      TimeTravaler.advanceTime(FIRST_PERIOD + SECOND_PERIOD + 1);

      await truffleAssert.reverts(
        staker.addSupply(AMOUNT, { from: RON }),
        "Unable to exec during this time period"
      );
    });
  });

  describe("getWithdrawReward", async () => {
    it("should stake 100, add supply 70, then getWithdrawReward 70", async () => {
      await staker.stake(AMOUNT, { from: ALICE });
      await staker.addSupply("70", { from: RON });

      assert.equal("70", await staker.getWithdrawReward(AMOUNT));
      assert.equal(
        ONE_ETH.sub(toBN(AMOUNT)).toString(),
        (await testCoin.balanceOf(ALICE)).toString()
      );
    });
  });

  describe("withdraw", async () => {
    it("should stake 100, add supply 100, then withdraw 200", async () => {
      await staker.stake(AMOUNT, { from: ALICE });
      await staker.addSupply(AMOUNT, { from: RON });

      TimeTravaler.advanceTime(FIRST_PERIOD + SECOND_PERIOD + 1);

      await staker.withdraw(ALICE);

      assert.equal("0", (await testCoin.balanceOf(staker.address)).toString());
      assert.equal("0", (await staker.totalStaked()).toString());
      assert.equal("0", await staker.totalSupply());
      assert.equal(true, await staker.withdrawalsCompleted(ALICE));
      assert.equal(
        ONE_ETH.add(toBN(AMOUNT)).toString(),
        (await testCoin.balanceOf(ALICE)).toString()
      );
    });

    it("should stake from RON and ALICE, then add supply 200, then withdraw all", async () => {
      await staker.stake(AMOUNT, { from: ALICE });
      await staker.stake(AMOUNT, { from: RON });
      await staker.addSupply(AMOUNT * 2, { from: RON });

      TimeTravaler.advanceTime(FIRST_PERIOD + SECOND_PERIOD + 1);

      await staker.withdraw(ALICE);
      await staker.withdraw(RON);

      assert.equal("0", (await testCoin.balanceOf(staker.address)).toString());
      assert.equal("0", await staker.totalStaked());
      assert.equal("0", (await staker.totalSupply()).toString());
      assert.equal(true, await staker.withdrawalsCompleted(ALICE));
      assert.equal(true, await staker.withdrawalsCompleted(RON));
      assert.equal(
        ONE_ETH.add(toBN(AMOUNT)).toString(),
        (await testCoin.balanceOf(ALICE)).toString()
      );
      assert.equal(
        ONE_ETH.sub(toBN(AMOUNT)).toString(),
        (await testCoin.balanceOf(RON)).toString()
      );
    });

    it("should stake 20 from ALICE, 30 from RON, 50 from BOB, then add supply 200, withdraw all", async () => {
      await staker.stake(20, { from: ALICE });
      await staker.stake(30, { from: RON });
      await staker.stake(50, { from: BOB });

      await testCoin.mint(200);
      await testCoin.approve(staker.address, 200);

      await staker.addSupply(200);

      TimeTravaler.advanceTime(FIRST_PERIOD + SECOND_PERIOD + 1);

      await staker.withdraw(ALICE);
      await staker.withdraw(RON);
      await staker.withdraw(BOB);

      assert.equal(
        ONE_ETH.add(toBN("40")).toString(),
        (await testCoin.balanceOf(ALICE)).toString()
      );
      assert.equal(
        ONE_ETH.add(toBN("60")).toString(),
        (await testCoin.balanceOf(RON)).toString()
      );
      assert.equal(
        ONE_ETH.add(toBN("100")).toString(),
        (await testCoin.balanceOf(BOB)).toString()
      );
    });

    it("should stake 33 from ALICE, 66 from RON, 1 from BOB, then add supply 77, withdraw all", async () => {
      let ETH_33 = ONE_ETH.mul(toBN("33"));
      let ETH_66 = ETH_33.mul(toBN("2"));
      let ETH_77 = ONE_ETH.mul(toBN("77"));

      await testCoin.mint(ETH_33, { from: ALICE });
      await testCoin.mint(ETH_66, { from: RON });

      await testCoin.approve(staker.address, ETH_33, { from: ALICE });
      await testCoin.approve(staker.address, ETH_66, { from: RON });
      await testCoin.approve(staker.address, ONE_ETH, { from: BOB });

      await staker.stake(ETH_33, { from: ALICE });
      await staker.stake(ETH_66, { from: RON });
      await staker.stake(ONE_ETH, { from: BOB });

      await testCoin.mint(ETH_77);
      await testCoin.approve(staker.address, ETH_77);

      await staker.addSupply(ETH_77);

      TimeTravaler.advanceTime(FIRST_PERIOD + SECOND_PERIOD + 1);

      await staker.withdraw(ALICE);
      await staker.withdraw(RON);
      await staker.withdraw(BOB);

      assert.equal(
        ONE_ETH.add(ETH_33)
          .add(toBN(web3.utils.toWei("25.41", "ether")))
          .toString(),
        (await testCoin.balanceOf(ALICE)).toString()
      );
      assert.equal(
        ONE_ETH.add(ETH_66)
          .add(toBN(web3.utils.toWei("50.82", "ether")))
          .toString(),
        (await testCoin.balanceOf(RON)).toString()
      );
      assert.equal(
        ONE_ETH.add(toBN(web3.utils.toWei("0.77", "ether"))).toString(),
        (await testCoin.balanceOf(BOB)).toString()
      );
      assert.equal("0", (await testCoin.balanceOf(staker.address)).toString());
    });

    it("should stake 100, supply 100, transfer 100, withdrow all", async () => {
      await staker.stake(AMOUNT, { from: ALICE });
      await staker.addSupply(AMOUNT, { from: RON });
      await testCoin.transfer(staker.address, AMOUNT, { from: BOB });

      TimeTravaler.advanceTime(FIRST_PERIOD + SECOND_PERIOD + 1);

      await staker.withdraw(ALICE);

      assert.equal(
        ONE_ETH.add(toBN("100")),
        (await testCoin.balanceOf(ALICE)).toString()
      );
      assert.equal(await testCoin.balanceOf(staker.address), AMOUNT);
    });

    it("should stake, then withdraw without reward", async () => {
      await staker.stake(AMOUNT, { from: ALICE });

      TimeTravaler.advanceTime(FIRST_PERIOD + SECOND_PERIOD + 1);

      await staker.withdraw(ALICE);

      assert.equal(
        ONE_ETH.toString(),
        (await testCoin.balanceOf(ALICE)).toString()
      );
      assert.equal("0", await testCoin.balanceOf(staker.address));
      assert.equal("0", await staker.totalStaked());
      assert.equal("0", await staker.totalSupply());
    });

    it("should revert when try withdraw before lock deadline", async () => {
      await staker.stake(AMOUNT, { from: ALICE });
      await staker.addSupply(AMOUNT, { from: RON });

      TimeTravaler.advanceTime(FIRST_PERIOD + 1);

      await truffleAssert.reverts(
        staker.withdraw(ALICE),
        "[E-21]: Unable to exec during this time period"
      );
    });

    it("should revert when try to withdraw several times", async () => {
      await staker.stake(AMOUNT, { from: ALICE });
      await staker.addSupply(AMOUNT, { from: RON });
      await testCoin.transfer(staker.address, AMOUNT, { from: BOB });

      TimeTravaler.advanceTime(FIRST_PERIOD + SECOND_PERIOD + 1);

      await staker.withdraw(ALICE);

      await truffleAssert.reverts(
        staker.withdraw(ALICE),
        "[E-22]: Withdrawal already completed"
      );
    });
  });

  describe("transferOverTokens", async () => {
    it("should transfer 100 to contract.address, then owner call transferOverTokens", async () => {
      await testCoin.transfer(staker.address, AMOUNT, { from: ALICE });
      assert.equal(
        AMOUNT,
        (await testCoin.balanceOf(staker.address)).toString()
      );
      assert.equal("0", await staker.totalStaked());
      assert.equal("0", await staker.totalSupply());
      assert.equal("0", await staker.usersStaked(ALICE));

      await staker.transferOverTokens(testCoin.address, OWNER, AMOUNT);

      assert.equal("0", (await testCoin.balanceOf(staker.address)).toString());
      assert.equal(AMOUNT, (await testCoin.balanceOf(OWNER)).toString());
    });

    it("should transfer another ERC20 token", async () => {
      let anotherToken = await TestCoin.new();

      await anotherToken.mint(AMOUNT);
      await anotherToken.transfer(staker.address, AMOUNT);

      await staker.transferOverTokens(anotherToken.address, OWNER, AMOUNT);

      assert.equal(await anotherToken.balanceOf(OWNER), AMOUNT);
    });

    it("should stake, supply, transfer, transfer from contract more then available", async () => {
      await testCoin.transfer(staker.address, AMOUNT, { from: BOB });

      await staker.stake(AMOUNT, { from: ALICE });
      await staker.addSupply(AMOUNT, { from: RON });

      await staker.transferOverTokens(testCoin.address, OWNER, AMOUNT * 3);

      assert.equal(AMOUNT, await testCoin.balanceOf(OWNER));
      assert.equal(
        (await testCoin.balanceOf(staker.address)).toString(),
        AMOUNT * 2
      );
      assert.equal(AMOUNT, await staker.totalSupply());
      assert.equal(AMOUNT, await staker.totalStaked());
    });
  });
});
