const Stake = artifacts.require("Stake.sol");
const Factory = artifacts.require("Factory.sol");
const TestCoin = artifacts.require("ERC20Mock");
const truffleAssert = require("truffle-assertions");
const Reverter = require("./helpers/reverter");

contract("Factory", async (accounts) => {
  const OWNER = accounts[0];
  const ALICE = accounts[1];
  const RON = accounts[2];
  const reverter = new Reverter(web3);
  const FIRST_PERIOD = 1000 * 60;
  const SECOND_PERIOD = FIRST_PERIOD * 5;

  let factory;
  let testCoin;

  let timestamp = async () => {
    return (await web3.eth.getBlock("latest"))["timestamp"];
  };

  before("setup", async () => {
    factory = await Factory.new();
    testCoin = await TestCoin.new();
    reverter.snapshot();
  });

  afterEach("revert", reverter.revert);

  describe("deployStakingContract", async () => {
    it("should create new staking contract", async () => {
      let response = await factory.deployStakingContract(
        (await timestamp()) + FIRST_PERIOD,
        (await timestamp()) + SECOND_PERIOD,
        testCoin.address
      );
      let addr = response.logs[2].args["_addr"];
      let staker = await Stake.at(addr);
      assert.equal("0", await staker.totalStaked());
    });

    it("should create staking contract and assert emited event", async () => {
      let response = await factory.deployStakingContract(
        (await timestamp()) + FIRST_PERIOD,
        (await timestamp()) + SECOND_PERIOD,
        testCoin.address
      );
      assert.equal(response.logs[2]["event"], "StakingDeployed");
    });

    it("should revert when create contract with uncorrect deadlines", async () => {
      await truffleAssert.reverts(
        factory.deployStakingContract(
          await timestamp(),
          (await timestamp()) - 1,
          testCoin.address
        ),
        "Incorrect timestamps"
      );
    });
  });
});
