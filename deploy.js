const Factory = artifacts.require("Factory.sol");
//const Stake = artifacts.require('Stake.sol');

module.exports = async (callback) => {
  console.log("Begin deploy!\n");
  try {
    //const stake = await Stake.new();
    const factory = await Factory.new();

    //console.log('stake: ', stake.address);
    console.log("factory: ", factory.address);
    console.log("\nDone!");
  } catch (e) {
    console.log(e.message);
  }

  callback();
};
