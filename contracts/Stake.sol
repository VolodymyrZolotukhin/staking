//SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.6;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./interface/IStake.sol";

contract Stake is Ownable, IStake {
    /// @notice Token amount staked on contract
    uint256 public totalStaked;
    /// @notice Token amount of reward pool
    uint256 public totalSupply;
    /// @notice Mapping of user address to amount of staked tokens
    mapping(address => uint256) public usersStaked;
    /// @notice Mapping of user address to bool (true if withdrawal completed)
    mapping(address => bool) public withdrawalsCompleted;
    /// @notice Deadline of stake period
    uint64 public stakeDeadline;
    /// @notice Deadline of lock period
    uint64 public lockDeadline;
    /// @notice Token that stake in contract
    IERC20 public token;

    /// @notice Check that time of period isn't out
    /// @param _deadline deadline of period
    modifier beforePeriodDeadline(uint256 _deadline) {
        require(
            block.timestamp <= _deadline,
            "[E-20]: Unable to exec during this time period"
        );
        _;
    }

    /// @param _stakeDeadline deadline of the first(stake) period
    /// @param _lockDeadline deadline of the second period
    /// @param _tokenAddress address of token, that uses for stake
    constructor(
        uint64 _stakeDeadline,
        uint64 _lockDeadline,
        address _tokenAddress
    ) {
        require(_stakeDeadline < _lockDeadline, "Incorrect timestamps");

        stakeDeadline = _stakeDeadline;
        lockDeadline = _lockDeadline;
        token = IERC20(_tokenAddress);
    }

    /// @notice function for stake tokens
    /// @param _amount amount of tokens to stake
    function stake(uint256 _amount)
        external
        override
        beforePeriodDeadline(stakeDeadline)
    {
        token.transferFrom(msg.sender, address(this), _amount);

        totalStaked += _amount;
        usersStaked[msg.sender] += _amount;
    }

    /// @notice fucntion for add tokens to reward pool
    /// @param _amount amount of tokens to add to reward pool
    function addSupply(uint256 _amount)
        external
        override
        beforePeriodDeadline(lockDeadline)
    {
        token.transferFrom(msg.sender, address(this), _amount);
        totalSupply += _amount;
    }

    /// @notice function to send reward and unstaked tokens to target account
    /// @param _target address to transfer reward and unstaked tokens
    function withdraw(address _target) external override {
        require(
            block.timestamp > lockDeadline,
            "[E-21]: Unable to exec during this time period"
        );
        require(
            !withdrawalsCompleted[_target],
            "[E-22]: Withdrawal already completed"
        );

        uint256 _senderStaked = usersStaked[_target];
        uint256 _reward = getWithdrawReward(_senderStaked);

        withdrawalsCompleted[_target] = true;

        totalSupply -= _reward;
        totalStaked -= _senderStaked;

        token.transfer(_target, _reward + _senderStaked);
    }

    /// @notice function to get mistakenly sent tokens from account
    /// @param _token IERC20 token, that will be send
    /// @param _to target address to send tokens
    /// @param _amount amount of tokens for send
    function transferOverTokens(
        IERC20 _token,
        address _to,
        uint256 _amount
    ) external override onlyOwner {
        if (address(_token) == address(token)) {
            uint256 _over = _token.balanceOf(address(this)) -
                (totalStaked + totalSupply);

            if (_amount > _over) {
                _amount = _over;
            }

            _token.transfer(_to, _amount);
        } else {
            _token.transfer(_to, _amount);
        }
    }

    /// @notice function for getting reward of user
    /// @param staked value of staked coins
    /// @return value of reward
    function getWithdrawReward(uint256 staked) public view returns (uint256) {
        uint256 reward = ((staked * totalSupply) / totalStaked);
        uint256 balance = token.balanceOf(address(this));
        return reward > balance ? balance : reward;
    }
}
