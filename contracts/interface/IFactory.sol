//SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.6;

interface IFactory {
    event StakingDeployed(address _addr);

    /// @notice function deploy new stake contract to net
    /// @param _stakeDeadline deadline of the first(stake) period
    /// @param _lockDeadline deadline of the second period
    /// @param _token address of token, that uses for stake
    /// @return address of new stake contract
    function deployStakingContract(
        uint64 _stakeDeadline,
        uint64 _lockDeadline,
        address _token
    ) external returns (address);
}
