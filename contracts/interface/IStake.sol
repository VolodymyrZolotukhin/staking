//SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.6;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IStake {
    /// @notice function for stake tokens
    /// @param _amount amount of tokens to stake
    function stake(uint256 _amount) external;

    /// @notice fucntion for add tokens to reward pool
    /// @param _amount amount of tokens to add to reward pool
    function addSupply(uint256 _amount) external;

    /// @notice function to send reward and unstaked tokens to target account
    /// @param _target address to transfer reward and unstaked tokens
    function withdraw(address _target) external;

    /// @notice function to get mistakenly sent tokens from account
    /// @param _token IERC20 token, that will be send
    /// @param _to target address to send tokens
    /// @param _amount amount of tokens for send
    function transferOverTokens(
        IERC20 _token,
        address _to,
        uint256 _amount
    ) external;
}
