//SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.6;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./Stake.sol";
import "./interface/IFactory.sol";

contract Factory is Ownable, IFactory {
    /// @notice function deploy new stake contract to net
    /// @param _stakeDeadline deadline of the first(stake) period
    /// @param _lockDeadline deadline of the second period
    /// @param _token address of token, that uses for stake
    /// @return address of new stake contract
    function deployStakingContract(
        uint64 _stakeDeadline,
        uint64 _lockDeadline,
        address _token
    ) external override onlyOwner returns (address) {
        Stake _newStake = new Stake(_stakeDeadline, _lockDeadline, _token);
        _newStake.transferOwnership(msg.sender);

        emit StakingDeployed(address(_newStake));

        return address(_newStake);
    }
}
